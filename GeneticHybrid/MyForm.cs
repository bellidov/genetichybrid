﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GeneticHybrid
{
    public partial class MyForm : Form
    {
        public MyForm()
        {
            InitializeComponent();
            g = Graphics.FromHwnd(panel.Handle);
            p = new Pen(Color.Red);
        }

        Graphics g;
        Pen p;
        IDrawer d;

        double[] a; // min (left) values for args 
        double[] b; // max (right) values for args
        IFunction f;
        IFormula formula;
        GA finder;
        int dim;
        double[] minArg;
        double minValue;

        private void SetDataToGen()
        {
            formula = new formula(InputFunction.Text);
            f = new MultiLipshevFun(formula);
            //get data from form and set to a, b arrays
            string[] sa = new string[] { x1_min.Text, x2_min.Text, x3_min.Text, x4_min.Text, textBox2.Text, textBox4.Text, textBox6.Text, textBox8.Text, textBox10.Text, textBox12.Text, textBox14.Text };
            string[] sb = new string[] { x1_max.Text, x2_max.Text, x3_max.Text, x4_max.Text, textBox1.Text, textBox3.Text, textBox5.Text, textBox7.Text, textBox9.Text, textBox11.Text, textBox13.Text };
            double[] _a = Array.ConvertAll(sa, new Converter<string, double>(Double.Parse));
            double[] _b = Array.ConvertAll(sb, new Converter<string, double>(Double.Parse));
            dim = f.getDim();
            a = new double[dim];
            b = new double[dim];
            Array.Copy(_a, a, dim);
            Array.Copy(_b, b, dim);

            //create a object Genetic Algoritm
            finder = new GA(a, b, Convert.ToInt32(generations.Text), Convert.ToInt32(populationSize.Text));

            //set genetic operators
            startTime = DateTime.Now;
            String fileName;

            IGeneticOperator LocalAdaptation = null;

            if (checkBox1.Checked)
            {
                LocalAdaptation = new LocalAdaptation(a, b);
                fileName = pathToSave + "GA_Hybrid_" + startTime.ToString("hh_mm_ss") + ".txt";
            }
            else
            {
                LocalAdaptation = new NothingToDo();
                fileName = pathToSave + "\\GA_Clasic_" + startTime.ToString("hh_mm_ss") + ".txt";
            }

            Console.WriteLine(fileName);

            finder.setOperators(new ArithmeticCrossover(Convert.ToInt32(crossingRate.Text)),
                                new Mutation(Convert.ToInt32(mutationRate.Text), a, b),
                                LocalAdaptation,
                                new Selector(Convert.ToInt32(populationSize.Text)),
                                new WriterLog(fileName, startTime));


            
        }

        DateTime startTime;

        


        private void RunButton_Click(object sender, EventArgs e)
        {
            SetDataToGen();

            double[] optArg = finder.FindMinArg(f);
            minArg = optArg;
            minValue = f.getValue(minArg);

            // vyvod informatsii na ekran
            MyArgs.Text = "";
            foreach (var v in formula.getArgs())
            {
                MyArgs.Text += v + "; ";
            }
            minArgTextBox.Text = "";
            foreach (var v in optArg)
            {
                minArgTextBox.Text += Math.Round(v, 2).ToString() + "; ";
            }
            minValueTextBox.Text = Math.Round(f.getValue(optArg), 2).ToString();       

            //set Xmin, Xmax, Ymin, Ymax to draw
            XminValue.Text = a[indexToDraw].ToString();
            XmaxValue.Text = b[indexToDraw].ToString();
            YmaxValue.Text = "0,1";
            if (YminKey)
            {
                Ymin = minValue - 0.3 * Math.Abs(minValue);
                YminValue.Text = Ymin.ToString();
                YminKey = false;
            }

            // comboBox
            VarComboBox.Items.Clear();
            foreach(var v in formula.getArgs()){
                VarComboBox.Items.Add(v);
            }
            VarComboBox.SelectedItem = formula.getArgs()[0];

            readyToDraw = true;
        }


        ///////////////////////////
        /////// Draw result ///////
        ///////////////////////////
        bool YminKey = true; // ustanavlivaet fiksirovannoe znachenie Ymin tolko 1 raz
        int indexToDraw = 0;
        double[] borders; // {Xmin, Xmax, Ymin, Ymax}
        double Xmin = 0, Xmax = 0, Ymin = 0, Ymax = 0;

        bool readyToDraw = false;

        private void DrawButton_Click(object sender, EventArgs e)
        {
            if (!readyToDraw)
                return;

            if (g != null)
                g.Clear(Color.White);
            d = new Drawer(g, p);

            SetDataToDraw();
            finder.Draw(d, indexToDraw, borders);
        }

        private void SetDataToDraw()
        {
            indexToDraw = VarComboBox.SelectedIndex;

            Ymin = Convert.ToDouble(YminValue.Text);
            Ymax = Convert.ToDouble(YmaxValue.Text);
            Xmin = Convert.ToDouble(XminValue.Text);
            Xmax = Convert.ToDouble(XmaxValue.Text);

            borders = new double[] { Xmin, Xmax, Ymin,  Ymax};
        }

        private void VarComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            indexToDraw = VarComboBox.SelectedIndex;
            XminValue.Text = a[indexToDraw].ToString();
            XmaxValue.Text = b[indexToDraw].ToString();
        }

        private void InputFunction_TextChanged(object sender, EventArgs e)
        {
            YminKey = true; // znachit, nuzhno budet snova fiksirovat minimalnoe znachenie dlia grafiki Ymin
        }

        private void x1_min_TextChanged(object sender, EventArgs e)
        {
            YminKey = true;
        }

        private void x1_max_TextChanged(object sender, EventArgs e)
        {
            YminKey = true;
        }

        private void x2_min_TextChanged(object sender, EventArgs e)
        {
            YminKey = true;
        }

        private void x2_max_TextChanged(object sender, EventArgs e)
        {
            YminKey = true;
        }

        private void x3_min_TextChanged(object sender, EventArgs e)
        {
            YminKey = true;
        }

        private void x3_max_TextChanged(object sender, EventArgs e)
        {
            YminKey = true;
        }

        private void x4_min_TextChanged(object sender, EventArgs e)
        {
            YminKey = true;
        }

        private void x4_max_TextChanged(object sender, EventArgs e)
        {
            YminKey = true;
        }


        FolderBrowserDialog savef = new FolderBrowserDialog();
        String pathToSave;
        private void button1_Click(object sender, EventArgs e)
        {
            if (savef.ShowDialog() == DialogResult.OK)
            {
                pathToSave = savef.SelectedPath;
            }
        }
    }
}
