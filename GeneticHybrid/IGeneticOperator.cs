﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace GeneticHybrid
{
    public interface IGeneticOperator
    {
        List<IGenotype> getPopulation(List<IGenotype> population, IFunction f);
    }

    public class Crossover : IGeneticOperator
    {
        private int crossingRate;

        public Crossover()
        {

        }

        public Crossover(int crossingRate)
        {
            this.crossingRate = crossingRate;
        }

        public virtual List<IGenotype> getPopulation(List<IGenotype> population, IFunction f)
        {
            int L = population[0].getGenotype().Length;
            int N = population.Count;
            int Fdim = f.getDim();
            int cross;

            Random r = new Random();
            cross = r.Next(1, L); // sluchainaya tochka dlia skreshivania

            double[] buff = new double[L];
            for (int k = 0; k < N - 1; k = k + 2) // beru po 2 osobi i skreshivayu ix
            {
                double[] child1 = new double[Fdim];
                double[] child2 = new double[Fdim];

                for (int i = 0; i < cross; i++)
                {
                    child1[i] = population[k].getGenotype()[i];
                    child2[i] = population[k + 1].getGenotype()[i];
                }
                for (int i = cross; i < L; i++)
                {
                    child1[i] = population[k + 1].getGenotype()[i];
                    child2[i] = population[k].getGenotype()[i];
                }

                population.Add(new Genotype(child1, f));
                population.Add(new Genotype(child2, f));
            }
            return population;
        }
    }

    public class Mutation : IGeneticOperator
    {
        private int mutationRate;
        double[] a, b;


        public Mutation(int mutationRate, double[] a, double[] b)
        {
            this.a = a;
            this.b = b;
            this.mutationRate = mutationRate;
        }

        public List<IGenotype> getPopulation(List<IGenotype> population, IFunction f)
        {
            int L = population[0].getGenotype().Length; // dlina genotype
            int N = population.Count;
            int Fdim = f.getDim();

            Random r = new Random();
            int count = 0;
            int max = (int)(mutationRate * N / 100);

            while(count < max)
            {
                int ip = r.Next(0, N); // index dlia populiatsii
                int ig = r.Next(0, L); // index dlia genotype
                double value = r.NextDouble() * (b[ig] - a[ig]) + a[ig]; // sluchainoe razreshennoe znachenie dlia gena

                double[] genotemp = new double[Fdim];
                for (int i = 0; i < Fdim; i++)
                {
                    genotemp[i] = population[ip].getGenotype()[i]; // sozdayu mutirovannij gen
                }
                genotemp[ig] = value; // sobstvenno, mutatsia

                population.Add(new Genotype(genotemp, f));

                count++;
            }
            return population;
        }
    }

    public class Selector : IGeneticOperator
    {
        private int N; // nachalniy zadanniy razmer populiatsii

        public Selector(int N)
        {
            this.N = N;
        }

        public List<IGenotype> getPopulation(List<IGenotype> population, IFunction f)
        {
            population.Sort(new comp());
            population.RemoveRange(N, population.Count-N);
            return population;
        }
    }


    public class ArithmeticCrossover : Crossover
    {
        private int crossingRate;

        public ArithmeticCrossover(int crossingRate)
        {
            this.crossingRate = crossingRate;
        }

        public override List<IGenotype> getPopulation(List<IGenotype> population, IFunction f)
        {
            int L = population[0].getGenotype().Length;
            int N = population.Count;
            int Fdim = f.getDim();
            double alpha;
            int max = (int)(this.crossingRate * N / 100); // maximalnoe kolichestvo brachnix par

            Random r = new Random();
            alpha = r.NextDouble(); // sluchainaya tochka dlia skreshivania

            for (int k = 0; k < max - 1; k = k + 2) // beru po 2 osobi i skreshivayu ix
            {
                int n = 0, m = 0; // indeksi dlia sluchainix roditelei
                n = new Random().Next(0, population.Count);
                m = new Random().Next(0, population.Count);

                double[] ancestor1 = new double[Fdim];
                double[] ancestor2 = new double[Fdim];

                double[] parent1 = population[n].getGenotype();
                double[] parent2 = population[m].getGenotype();

                for (int i = 0; i < L; i++)
                {
                    ancestor1[i] = alpha*parent1[i] + (1-alpha)*parent2[i];
                    ancestor2[i] = (1-alpha) * parent1[i] + alpha * parent2[i];
                }

                population.Add(new Genotype(ancestor1, f));
                population.Add(new Genotype(ancestor2, f));
            }
            return population;
        }
    }

    public class WriterLog : IGeneticOperator
    {
        private DateTime current, start;
        private string fileName;
     

        public WriterLog(String fileName, DateTime start)
        {
            this.start = start;
            this.fileName = fileName;
            if (File.Exists(fileName))
            {
                File.Delete(fileName);
            }
        }

        public List<IGenotype> getPopulation(List<IGenotype> population, IFunction f)
        {
        //    fileName = "pios.txt";
            using (StreamWriter writer = new StreamWriter(fileName, true, Encoding.Default))
            {
                double bestFitness = getBestFitness(population);
                double averageFitness = getFitnessMedium(population);
                current = DateTime.Now;
                double interval = current.Subtract(start).TotalMilliseconds;
                writer.WriteLine("{0} {1} {2} {3}", bestFitness, averageFitness, interval, DateTime.Now.ToString("HH:mm:ss:fff"));
            }
            return population;
        }

        // vychisliaet srednuyu prisposoblennost dlia kazhdoi populiatsii
        private double getFitnessMedium(IEnumerable<IGenotype> population)
        {
            return population.Average(genotype => genotype.getRang());
        }

        private double getBestFitness(IEnumerable<IGenotype> population)
        {
            return population.Min(genotype => genotype.getRang());
        }
    }

    class NothingToDo : IGeneticOperator
    {

        public List<IGenotype> getPopulation(List<IGenotype> population, IFunction f)
        {
            return population;
        }
    }

    class LocalAdaptation : IGeneticOperator
    {
        private IExtremunFinder finder;
        private int dim = 0;
        double[] a, b;

        public LocalAdaptation(double[] a, double[] b)
        {
            this.a = a;
            this.b = b;
        }

        public List<IGenotype> getPopulation(List<IGenotype> population, IFunction f)
        {
            List<IGenotype> popu = new List<IGenotype>(); // to chto budet na vyxode
           
            int count = 0;
            if (dim == 0)
            {
                dim = f.getDim();
            }

            // cycle proxodit po kazhdoi osobi v populiatsii
            while (count < population.Count)
            {
                double[] argMin = (double[])population[count].getGenotype().Clone(); // delayu kopiu tekushego massiva
                IGenotype genoMin; // budet dobavliatsa v populiatsiu popu

                // cycl proxodit po kazhdomu elementu tekushego genotypa
                for (int i = 0; i < dim; i++)
                {
                    genoMin = new Genotype(argMin, f);

                    double[] argTemp = (double[])argMin.Clone(); // vremenno, dlia sravnenia
                    finder = new Strongin(a[i], b[i], argTemp, i, 0.5); // po ocheredi uluchaetsa kazhdiy element genotypa
                    argTemp[i] = finder.argMin(f);
                    IGenotype genoTemp = new Genotype(argTemp, f);

                    // uslovie ostanova
                    if (genoTemp.getRang() <= genoMin.getRang())
                    {
                        //esli bylo ulushenie, to soxraniayu ego
                        argMin = (double[])argTemp.Clone();
                    }
                    else
                    {  
                        //esli net ulushenia, to vyxozhu is tsikla chtoby pereiti k sleduyushei osobi
                        break;
                    }
                    
                }

                genoMin = new Genotype(argMin, f);
                popu.Add(genoMin);
                count++;
            }
            //fin de ciclo

            return popu;
        }
    }
}
