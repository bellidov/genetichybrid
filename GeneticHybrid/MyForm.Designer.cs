﻿namespace GeneticHybrid
{
    partial class MyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.InputFunction = new System.Windows.Forms.TextBox();
            this.RunButton = new System.Windows.Forms.Button();
            this.generations = new System.Windows.Forms.TextBox();
            this.mutationRate = new System.Windows.Forms.TextBox();
            this.crossingRate = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.populationSize = new System.Windows.Forms.TextBox();
            this.minArgTextBox = new System.Windows.Forms.TextBox();
            this.minValueTextBox = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.x1_min = new System.Windows.Forms.TextBox();
            this.x1_max = new System.Windows.Forms.TextBox();
            this.x2_max = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.x2_min = new System.Windows.Forms.TextBox();
            this.x3_max = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.x3_min = new System.Windows.Forms.TextBox();
            this.x4_max = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.x4_min = new System.Windows.Forms.TextBox();
            this.MyArgs = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.panel = new System.Windows.Forms.Panel();
            this.DrawButton = new System.Windows.Forms.Button();
            this.YmaxLabel = new System.Windows.Forms.Label();
            this.YmaxValue = new System.Windows.Forms.TextBox();
            this.YminLabel = new System.Windows.Forms.Label();
            this.YminValue = new System.Windows.Forms.TextBox();
            this.XminLabel = new System.Windows.Forms.Label();
            this.XminValue = new System.Windows.Forms.TextBox();
            this.XmaxLabel = new System.Windows.Forms.Label();
            this.XmaxValue = new System.Windows.Forms.TextBox();
            this.VarComboBox = new System.Windows.Forms.ComboBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // InputFunction
            // 
            this.InputFunction.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.InputFunction.Location = new System.Drawing.Point(30, 12);
            this.InputFunction.Name = "InputFunction";
            this.InputFunction.Size = new System.Drawing.Size(399, 26);
            this.InputFunction.TabIndex = 0;
            this.InputFunction.Text = "(x1*x2+x6-x3+x4*x5*x7-x8^2-x9^3+x10*x11)*cos(x1+2*x2-x3-50*x4+2*x5-25*x6+3*x7-x8+" +
    "7*x9+2*x10-5*x11+45)";
            this.InputFunction.TextChanged += new System.EventHandler(this.InputFunction_TextChanged);
            // 
            // RunButton
            // 
            this.RunButton.Location = new System.Drawing.Point(367, 244);
            this.RunButton.Name = "RunButton";
            this.RunButton.Size = new System.Drawing.Size(125, 73);
            this.RunButton.TabIndex = 2;
            this.RunButton.Text = "Calculate";
            this.RunButton.UseVisualStyleBackColor = true;
            this.RunButton.Click += new System.EventHandler(this.RunButton_Click);
            // 
            // generations
            // 
            this.generations.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.generations.Location = new System.Drawing.Point(349, 151);
            this.generations.Name = "generations";
            this.generations.Size = new System.Drawing.Size(80, 26);
            this.generations.TabIndex = 4;
            this.generations.Text = "500000";
            // 
            // mutationRate
            // 
            this.mutationRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.mutationRate.Location = new System.Drawing.Point(349, 87);
            this.mutationRate.Name = "mutationRate";
            this.mutationRate.Size = new System.Drawing.Size(80, 26);
            this.mutationRate.TabIndex = 5;
            this.mutationRate.Text = "25";
            // 
            // crossingRate
            // 
            this.crossingRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.crossingRate.Location = new System.Drawing.Point(349, 119);
            this.crossingRate.Name = "crossingRate";
            this.crossingRate.Size = new System.Drawing.Size(80, 26);
            this.crossingRate.TabIndex = 6;
            this.crossingRate.Text = "100";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(204, 90);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 20);
            this.label1.TabIndex = 7;
            this.label1.Text = "mutation rate (%)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(204, 122);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(139, 20);
            this.label2.TabIndex = 8;
            this.label2.Text = "crossing rate (%)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(204, 154);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(111, 20);
            this.label3.TabIndex = 9;
            this.label3.Text = "count stopper";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(68, 61);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 20);
            this.label4.TabIndex = 17;
            this.label4.Text = "min";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(116, 61);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 20);
            this.label5.TabIndex = 18;
            this.label5.Text = "max";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.Location = new System.Drawing.Point(204, 58);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(121, 20);
            this.label10.TabIndex = 24;
            this.label10.Text = "population size";
            // 
            // populationSize
            // 
            this.populationSize.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.populationSize.Location = new System.Drawing.Point(349, 55);
            this.populationSize.Name = "populationSize";
            this.populationSize.Size = new System.Drawing.Size(80, 26);
            this.populationSize.TabIndex = 23;
            this.populationSize.Text = "20";
            // 
            // minArgTextBox
            // 
            this.minArgTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.minArgTextBox.Location = new System.Drawing.Point(135, 394);
            this.minArgTextBox.Name = "minArgTextBox";
            this.minArgTextBox.Size = new System.Drawing.Size(240, 26);
            this.minArgTextBox.TabIndex = 25;
            // 
            // minValueTextBox
            // 
            this.minValueTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.minValueTextBox.Location = new System.Drawing.Point(135, 435);
            this.minValueTextBox.Name = "minValueTextBox";
            this.minValueTextBox.Size = new System.Drawing.Size(89, 26);
            this.minValueTextBox.TabIndex = 26;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.Location = new System.Drawing.Point(45, 435);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(80, 20);
            this.label11.TabIndex = 28;
            this.label11.Text = "min value";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label12.Location = new System.Drawing.Point(45, 394);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(65, 20);
            this.label12.TabIndex = 27;
            this.label12.Text = "min arg";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(26, 87);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(26, 20);
            this.label6.TabIndex = 30;
            this.label6.Text = "x1";
            // 
            // x1_min
            // 
            this.x1_min.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.x1_min.Location = new System.Drawing.Point(72, 84);
            this.x1_min.Name = "x1_min";
            this.x1_min.Size = new System.Drawing.Size(41, 26);
            this.x1_min.TabIndex = 29;
            this.x1_min.Text = "-4";
            this.x1_min.TextChanged += new System.EventHandler(this.x1_min_TextChanged);
            // 
            // x1_max
            // 
            this.x1_max.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.x1_max.Location = new System.Drawing.Point(119, 84);
            this.x1_max.Name = "x1_max";
            this.x1_max.Size = new System.Drawing.Size(37, 26);
            this.x1_max.TabIndex = 31;
            this.x1_max.Text = "4";
            this.x1_max.TextChanged += new System.EventHandler(this.x1_max_TextChanged);
            // 
            // x2_max
            // 
            this.x2_max.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.x2_max.Location = new System.Drawing.Point(119, 114);
            this.x2_max.Name = "x2_max";
            this.x2_max.Size = new System.Drawing.Size(37, 26);
            this.x2_max.TabIndex = 34;
            this.x2_max.Text = "4";
            this.x2_max.TextChanged += new System.EventHandler(this.x2_max_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(26, 117);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(26, 20);
            this.label7.TabIndex = 33;
            this.label7.Text = "x2";
            // 
            // x2_min
            // 
            this.x2_min.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.x2_min.Location = new System.Drawing.Point(72, 114);
            this.x2_min.Name = "x2_min";
            this.x2_min.Size = new System.Drawing.Size(41, 26);
            this.x2_min.TabIndex = 32;
            this.x2_min.Text = "-4";
            this.x2_min.TextChanged += new System.EventHandler(this.x2_min_TextChanged);
            // 
            // x3_max
            // 
            this.x3_max.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.x3_max.Location = new System.Drawing.Point(119, 144);
            this.x3_max.Name = "x3_max";
            this.x3_max.Size = new System.Drawing.Size(37, 26);
            this.x3_max.TabIndex = 37;
            this.x3_max.Text = "9";
            this.x3_max.TextChanged += new System.EventHandler(this.x3_max_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(26, 147);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(26, 20);
            this.label8.TabIndex = 36;
            this.label8.Text = "x3";
            // 
            // x3_min
            // 
            this.x3_min.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.x3_min.Location = new System.Drawing.Point(72, 144);
            this.x3_min.Name = "x3_min";
            this.x3_min.Size = new System.Drawing.Size(41, 26);
            this.x3_min.TabIndex = 35;
            this.x3_min.Text = "0";
            this.x3_min.TextChanged += new System.EventHandler(this.x3_min_TextChanged);
            // 
            // x4_max
            // 
            this.x4_max.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.x4_max.Location = new System.Drawing.Point(119, 175);
            this.x4_max.Name = "x4_max";
            this.x4_max.Size = new System.Drawing.Size(37, 26);
            this.x4_max.TabIndex = 40;
            this.x4_max.Text = "9";
            this.x4_max.TextChanged += new System.EventHandler(this.x4_max_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(26, 178);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(26, 20);
            this.label9.TabIndex = 39;
            this.label9.Text = "x4";
            // 
            // x4_min
            // 
            this.x4_min.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.x4_min.Location = new System.Drawing.Point(72, 175);
            this.x4_min.Name = "x4_min";
            this.x4_min.Size = new System.Drawing.Size(41, 26);
            this.x4_min.TabIndex = 38;
            this.x4_min.Text = "0";
            this.x4_min.TextChanged += new System.EventHandler(this.x4_min_TextChanged);
            // 
            // MyArgs
            // 
            this.MyArgs.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MyArgs.Location = new System.Drawing.Point(135, 354);
            this.MyArgs.Name = "MyArgs";
            this.MyArgs.Size = new System.Drawing.Size(240, 26);
            this.MyArgs.TabIndex = 41;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label13.Location = new System.Drawing.Point(32, 357);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(97, 20);
            this.label13.TabIndex = 42;
            this.label13.Text = "args names";
            // 
            // panel
            // 
            this.panel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel.Location = new System.Drawing.Point(564, 20);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(600, 400);
            this.panel.TabIndex = 43;
            // 
            // DrawButton
            // 
            this.DrawButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.DrawButton.Location = new System.Drawing.Point(864, 449);
            this.DrawButton.Name = "DrawButton";
            this.DrawButton.Size = new System.Drawing.Size(131, 41);
            this.DrawButton.TabIndex = 48;
            this.DrawButton.Text = "Draw";
            this.DrawButton.UseVisualStyleBackColor = true;
            this.DrawButton.Click += new System.EventHandler(this.DrawButton_Click);
            // 
            // YmaxLabel
            // 
            this.YmaxLabel.AutoSize = true;
            this.YmaxLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.YmaxLabel.Location = new System.Drawing.Point(508, 49);
            this.YmaxLabel.Name = "YmaxLabel";
            this.YmaxLabel.Size = new System.Drawing.Size(50, 20);
            this.YmaxLabel.TabIndex = 50;
            this.YmaxLabel.Text = "Ymax";
            // 
            // YmaxValue
            // 
            this.YmaxValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.YmaxValue.Location = new System.Drawing.Point(498, 20);
            this.YmaxValue.Name = "YmaxValue";
            this.YmaxValue.Size = new System.Drawing.Size(60, 26);
            this.YmaxValue.TabIndex = 49;
            this.YmaxValue.Text = "0,1";
            // 
            // YminLabel
            // 
            this.YminLabel.AutoSize = true;
            this.YminLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.YminLabel.Location = new System.Drawing.Point(494, 364);
            this.YminLabel.Name = "YminLabel";
            this.YminLabel.Size = new System.Drawing.Size(46, 20);
            this.YminLabel.TabIndex = 52;
            this.YminLabel.Text = "Ymin";
            // 
            // YminValue
            // 
            this.YminValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.YminValue.Location = new System.Drawing.Point(498, 387);
            this.YminValue.Name = "YminValue";
            this.YminValue.Size = new System.Drawing.Size(60, 26);
            this.YminValue.TabIndex = 51;
            // 
            // XminLabel
            // 
            this.XminLabel.AutoSize = true;
            this.XminLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.XminLabel.Location = new System.Drawing.Point(630, 434);
            this.XminLabel.Name = "XminLabel";
            this.XminLabel.Size = new System.Drawing.Size(47, 20);
            this.XminLabel.TabIndex = 54;
            this.XminLabel.Text = "Xmin";
            // 
            // XminValue
            // 
            this.XminValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.XminValue.Location = new System.Drawing.Point(564, 431);
            this.XminValue.Name = "XminValue";
            this.XminValue.Size = new System.Drawing.Size(60, 26);
            this.XminValue.TabIndex = 53;
            // 
            // XmaxLabel
            // 
            this.XmaxLabel.AutoSize = true;
            this.XmaxLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.XmaxLabel.Location = new System.Drawing.Point(1040, 437);
            this.XmaxLabel.Name = "XmaxLabel";
            this.XmaxLabel.Size = new System.Drawing.Size(51, 20);
            this.XmaxLabel.TabIndex = 56;
            this.XmaxLabel.Text = "Xmax";
            // 
            // XmaxValue
            // 
            this.XmaxValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.XmaxValue.Location = new System.Drawing.Point(1097, 431);
            this.XmaxValue.Name = "XmaxValue";
            this.XmaxValue.Size = new System.Drawing.Size(67, 26);
            this.XmaxValue.TabIndex = 55;
            // 
            // VarComboBox
            // 
            this.VarComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.VarComboBox.FormattingEnabled = true;
            this.VarComboBox.Location = new System.Drawing.Point(743, 456);
            this.VarComboBox.Name = "VarComboBox";
            this.VarComboBox.Size = new System.Drawing.Size(104, 28);
            this.VarComboBox.TabIndex = 57;
            this.VarComboBox.Text = "select var";
            this.VarComboBox.SelectedIndexChanged += new System.EventHandler(this.VarComboBox_SelectedIndexChanged);
            // 
            // textBox7
            // 
            this.textBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox7.Location = new System.Drawing.Point(119, 306);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(37, 26);
            this.textBox7.TabIndex = 81;
            this.textBox7.Text = "9";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label17.Location = new System.Drawing.Point(26, 309);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(26, 20);
            this.label17.TabIndex = 80;
            this.label17.Text = "x8";
            // 
            // textBox8
            // 
            this.textBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox8.Location = new System.Drawing.Point(72, 306);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(41, 26);
            this.textBox8.TabIndex = 79;
            this.textBox8.Text = "0";
            // 
            // textBox5
            // 
            this.textBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox5.Location = new System.Drawing.Point(119, 273);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(37, 26);
            this.textBox5.TabIndex = 78;
            this.textBox5.Text = "9";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label16.Location = new System.Drawing.Point(26, 276);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(26, 20);
            this.label16.TabIndex = 77;
            this.label16.Text = "x7";
            // 
            // textBox6
            // 
            this.textBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox6.Location = new System.Drawing.Point(72, 273);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(41, 26);
            this.textBox6.TabIndex = 76;
            this.textBox6.Text = "-9";
            // 
            // textBox3
            // 
            this.textBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox3.Location = new System.Drawing.Point(119, 241);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(37, 26);
            this.textBox3.TabIndex = 75;
            this.textBox3.Text = "9";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label15.Location = new System.Drawing.Point(26, 244);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(26, 20);
            this.label15.TabIndex = 74;
            this.label15.Text = "x6";
            // 
            // textBox4
            // 
            this.textBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox4.Location = new System.Drawing.Point(72, 241);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(41, 26);
            this.textBox4.TabIndex = 73;
            this.textBox4.Text = "0";
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox1.Location = new System.Drawing.Point(119, 209);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(37, 26);
            this.textBox1.TabIndex = 72;
            this.textBox1.Text = "9";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label14.Location = new System.Drawing.Point(26, 212);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(26, 20);
            this.label14.TabIndex = 71;
            this.label14.Text = "x5";
            // 
            // textBox2
            // 
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox2.Location = new System.Drawing.Point(72, 209);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(41, 26);
            this.textBox2.TabIndex = 70;
            this.textBox2.Text = "0";
            // 
            // textBox13
            // 
            this.textBox13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox13.Location = new System.Drawing.Point(269, 309);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(37, 26);
            this.textBox13.TabIndex = 90;
            this.textBox13.Text = "9";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label20.Location = new System.Drawing.Point(176, 312);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(35, 20);
            this.label20.TabIndex = 89;
            this.label20.Text = "x11";
            // 
            // textBox14
            // 
            this.textBox14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox14.Location = new System.Drawing.Point(222, 309);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(41, 26);
            this.textBox14.TabIndex = 88;
            this.textBox14.Text = "0";
            // 
            // textBox11
            // 
            this.textBox11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox11.Location = new System.Drawing.Point(269, 276);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(37, 26);
            this.textBox11.TabIndex = 87;
            this.textBox11.Text = "9";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label19.Location = new System.Drawing.Point(176, 279);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(35, 20);
            this.label19.TabIndex = 86;
            this.label19.Text = "x10";
            // 
            // textBox12
            // 
            this.textBox12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox12.Location = new System.Drawing.Point(222, 276);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(41, 26);
            this.textBox12.TabIndex = 85;
            this.textBox12.Text = "0";
            // 
            // textBox9
            // 
            this.textBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox9.Location = new System.Drawing.Point(269, 241);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(37, 26);
            this.textBox9.TabIndex = 84;
            this.textBox9.Text = "9";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label18.Location = new System.Drawing.Point(176, 244);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(26, 20);
            this.label18.TabIndex = 83;
            this.label18.Text = "x9";
            // 
            // textBox10
            // 
            this.textBox10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox10.Location = new System.Drawing.Point(222, 241);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(41, 26);
            this.textBox10.TabIndex = 82;
            this.textBox10.Text = "0";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox1.Location = new System.Drawing.Point(251, 198);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(154, 21);
            this.checkBox1.TabIndex = 92;
            this.checkBox1.Text = "Use Hybrid algoritm";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(29, 503);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(256, 46);
            this.button1.TabIndex = 93;
            this.button1.Text = "Path to save File";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // MyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1185, 561);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.textBox13);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.textBox14);
            this.Controls.Add(this.textBox11);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.textBox12);
            this.Controls.Add(this.textBox9);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.textBox10);
            this.Controls.Add(this.textBox7);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.textBox8);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.textBox6);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.VarComboBox);
            this.Controls.Add(this.XmaxLabel);
            this.Controls.Add(this.XmaxValue);
            this.Controls.Add(this.XminLabel);
            this.Controls.Add(this.XminValue);
            this.Controls.Add(this.YminLabel);
            this.Controls.Add(this.YminValue);
            this.Controls.Add(this.YmaxLabel);
            this.Controls.Add(this.YmaxValue);
            this.Controls.Add(this.DrawButton);
            this.Controls.Add(this.panel);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.MyArgs);
            this.Controls.Add(this.x4_max);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.x4_min);
            this.Controls.Add(this.x3_max);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.x3_min);
            this.Controls.Add(this.x2_max);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.x2_min);
            this.Controls.Add(this.x1_max);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.x1_min);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.minValueTextBox);
            this.Controls.Add(this.minArgTextBox);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.populationSize);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.crossingRate);
            this.Controls.Add(this.mutationRate);
            this.Controls.Add(this.generations);
            this.Controls.Add(this.RunButton);
            this.Controls.Add(this.InputFunction);
            this.Name = "MyForm";
            this.Text = "MyForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox InputFunction;
        private System.Windows.Forms.Button RunButton;
        private System.Windows.Forms.TextBox generations;
        private System.Windows.Forms.TextBox mutationRate;
        private System.Windows.Forms.TextBox crossingRate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox populationSize;
        private System.Windows.Forms.TextBox minArgTextBox;
        private System.Windows.Forms.TextBox minValueTextBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox x1_min;
        private System.Windows.Forms.TextBox x1_max;
        private System.Windows.Forms.TextBox x2_max;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox x2_min;
        private System.Windows.Forms.TextBox x3_max;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox x3_min;
        private System.Windows.Forms.TextBox x4_max;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox x4_min;
        private System.Windows.Forms.TextBox MyArgs;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.Button DrawButton;
        private System.Windows.Forms.Label YmaxLabel;
        private System.Windows.Forms.TextBox YmaxValue;
        private System.Windows.Forms.Label YminLabel;
        private System.Windows.Forms.TextBox YminValue;
        private System.Windows.Forms.Label XminLabel;
        private System.Windows.Forms.TextBox XminValue;
        private System.Windows.Forms.Label XmaxLabel;
        private System.Windows.Forms.TextBox XmaxValue;
        private System.Windows.Forms.ComboBox VarComboBox;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Button button1;
    }
}