﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticHybrid
{
    public interface IFunction
    {
        double getValue(double[] x);
        int getDim();
    }

    public class MultiLipshevFun : IFunction
    {
        IFormula fm;
        public MultiLipshevFun(IFormula fm)
        {
            this.fm = fm;
        }

        public double getValue(double[] x)
        {
            return fm.eval(x);
        }

        public int getDim()
        {
            return fm.getDim();
        }
    }
}
